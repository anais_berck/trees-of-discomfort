#pragma once
#include <cstdarg>
namespace PublishingHouse
{
  namespace RandomForest
  {
    class DecisionTree
    {
    public:
      const char* predict(float (*getObservationValue)(int), bool (*consider)(float, int, float))
      {
        if ((consider)((getObservationValue)(2), 2, 3.3)) {
          if ((consider)((getObservationValue)(0), 0, 5.1)) {
            if ((consider)((getObservationValue)(3), 3, 0.2)) {
              if ((consider)((getObservationValue)(3), 3, 0.1)) {
                return "Iris Setosa";
              }
              else {
                return "Iris Setosa";
              }
            }
            else {
              return "Iris Setosa";
            }
          }
          else {
            return "Iris Setosa";
          }
        }
        else {
          if ((consider)((getObservationValue)(3), 3, 1.8)) {
            if ((consider)((getObservationValue)(3), 3, 1.6)) {
              if ((consider)((getObservationValue)(0), 0, 6.1)) {
                if ((consider)((getObservationValue)(2), 2, 4.2)) {
                  if ((consider)((getObservationValue)(0), 0, 5.8)) {
                    if ((consider)((getObservationValue)(1), 1, 3.0)) {
                      if ((consider)((getObservationValue)(0), 0, 5.6)) {
                        if ((consider)((getObservationValue)(3), 3, 1.3)) {
                          if ((consider)((getObservationValue)(0), 0, 5.0)) {
                            return "Iris Versicolour";
                          }
                          else {
                            return "Iris Versicolour";
                          }
                        }
                        else {
                          return "Iris Versicolour";
                        }
                      }
                      else {
                        return "Iris Versicolour";
                      }
                    }
                    else {
                      return "Iris Versicolour";
                    }
                  }
                  else {
                    return "Iris Versicolour";
                  }
                }
                else {
                  return "Iris Versicolour";
                }
              }
              else {
                return "Iris Versicolour";
              }
            }
            else {
              if ((consider)((getObservationValue)(1), 1, 3.3)) {
                return "Iris Virginica";
              }
              else {
                return "Iris Versicolour";
              }
            }
          }
          else {
            if ((consider)((getObservationValue)(2), 2, 4.9)) {
              if ((consider)((getObservationValue)(0), 0, 6.0)) {
                return "Iris Versicolour";
              }
              else {
                return "Iris Virginica";
              }
            }
            else {
              return "Iris Virginica";
            }
          }
        }
      }
    private:
    };
  }
}