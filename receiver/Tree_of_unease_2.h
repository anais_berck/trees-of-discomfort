#pragma once
#include <cstdarg>
namespace PublishingHouse
{
  namespace RandomForest
  {
    class TreeOfUnease
    {
    public:
      const char* traverse(float (*getObservationValue)(int), bool (*considerWithUnease)(const char*, float, int, float))
      {
        if ((considerWithUnease)("I feel unease on classification...",(getObservationValue)(3), 3, 1.0)) {
          if ((considerWithUnease)("...about the need for models…",(getObservationValue)(1), 1, 3.0)) {
            if ((considerWithUnease)("...what is simple is always wrong. What is not is unusable. Paul Valéry 1942…",(getObservationValue)(0), 0, 4.4)) {
              return "Iris Setosa";
            }
            else {
              return "Iris Setosa";
            }
          }
          else {
            return "Iris Setosa";
          }
        }
        else {
          if ((considerWithUnease)("...about modeling from observations…",(getObservationValue)(3), 3, 1.8)) {
            if ((considerWithUnease)("...it seems that we are reducing plants to a few numbered characteristics…",(getObservationValue)(3), 3, 1.5)) {
              if ((considerWithUnease)("...why not apply the same to humans ?",(getObservationValue)(3), 3, 1.4)) {
                return "?";
              }
              else {
                return "Iris Versicolour";
              }
            }
            else {
              if ((considerWithUnease)("...is the practice of categorisation bad in itself ?",(getObservationValue)(1), 1, 3.0)) {
                return "?";
              }
              else {
                return "?";
              }
            }
          }
          else {
            return "Iris Virginica";
          }
        }
      }
    private:
    };
  }
}