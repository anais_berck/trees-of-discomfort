#pragma once
#include <cstdarg>
namespace PublishingHouse
{
  namespace RandomForest
  {
    class TreeOfUnease
    {
    public:
      const char* traverse(float (*getObservationValue)(int), bool (*considerWithUnease)(const char*, float, int, float))
      {
        if ((considerWithUnease)("But on the data of Meise…",(getObservationValue)(2), 2, 3.3)) {
          if ((considerWithUnease)("...and their digital portal...",(getObservationValue)(2), 2, 1.4)) {
            if ((considerWithUnease)("...the fields used to describe the specimens seem restrictive",(getObservationValue)(3), 3, 0.1)) {
              return "Iris Setosa";
            }
            else {
              return "Iris Setosa";
            }
          }
          else {
            return "Iris Setosa";
          }
        }
        else {
          if ((considerWithUnease)("...and the content of the dataset…",(getObservationValue)(2), 2, 4.8)) {
            if ((considerWithUnease)("...one of Meise’s expertise is coffee and they gather a lot of data about it…",(getObservationValue)(3), 3, 1.0)) {
              return "Iris Versicolour";
            }
            else {
              return "Iris Versicolour";
            }
          }
          else {
            if ((considerWithUnease)("...the repartition of the samples is quite telling...",(getObservationValue)(2), 2, 4.9)) {
              if ((considerWithUnease)("...1,5 % of type specimen were collected in Belgium, 22 % in RDC, 18 % in Brazil",(getObservationValue)(0), 0, 6.2)) {
                return "Iris Versicolour";
              }
              else {
                return "Iris Virginica";
              }
            }
            else {
              if ((considerWithUnease)("...and mirrors the history of Botany",(getObservationValue)(3), 3, 1.8)) {
                return "?";
              }
              else {
                return "Iris Virginica";
              }
            }
          }
        }
      }
    private:
    };
  }
}