#pragma once
#include <cstdarg>
namespace PublishingHouse
{
  namespace RandomForest
  {
    class TreeOfUnease
    {
    public:
      const char* traverse(float (*getObservationValue)(int), bool (*considerWithUnease)(const char*, float, int, float))
      {
        if ((considerWithUnease)("There is something with the Iris Dataset",(getObservationValue)(2), 2, 3.5)) {
          if ((considerWithUnease)("...its origin are troubling to say the least…",(getObservationValue)(1), 1, 4.0)) {
            if ((considerWithUnease)("...it was first published in the Annals of Eugenics… Is it wrong to display it ?",(getObservationValue)(1), 1, 3.0)) {
              if ((considerWithUnease)("",(getObservationValue)(2), 2, 1.4)) {
                return "Iris Setosa";
              }
              else {
                return "Iris Setosa";
              }
            }
            else {
              return "Iris Setosa";
            }
          }
          else {
            return "Iris Setosa";
          }
        }
        else {
          if ((considerWithUnease)("...species in this dataset are lineraly separable.. ",(getObservationValue)(3), 3, 1.8)) {
            if ((considerWithUnease)("...which make classification algorithms reach perfect accuracy…",(getObservationValue)(0), 0, 5.7)) {
              if ((considerWithUnease)("...this also explains why it is so popular in Machine Learning education.",(getObservationValue)(0), 0, 5.2)) {
                return "Iris Versicolour";
              }
              else {
                return "Iris Versicolour";
              }
            }
            else {
              return "Iris Versicolour";
            }
          }
          else {
            return "Iris Virginica";
          }
        }
      }
    private:
    };
  }
}