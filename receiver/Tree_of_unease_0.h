#pragma once
#include <cstdarg>
namespace PublishingHouse
{
  namespace RandomForest
  {
    class TreeOfUnease
    {
    public:
      const char* traverse(float (*getObservationValue)(int), bool (*considerWithUnease)(const char*, float, int, float))
      {
        if ((considerWithUnease)("I have a thought about the Meise herbarium...",(getObservationValue)(3), 3, 1.0)) {
          if ((considerWithUnease)("...about their « living herbarium »...",(getObservationValue)(3), 3, 0.2)) {
            if ((considerWithUnease)("...some of plants there come from Leopold II his garden:",(getObservationValue)(3), 3, 0.1)) {
              return "Iris Setosa";
            }
            else {
              return "Iris Setosa";
            }
          }
          else {
            return "Iris Setosa";
          }
        }
        else {
          if ((considerWithUnease)("...about the creation of its herbarium...",(getObservationValue)(2), 2, 4.8)) {
            if ((considerWithUnease)("...the basis of the Meise Collection is an herbarium of the plants from Brazil...",(getObservationValue)(2), 2, 4.6)) {
              if ((considerWithUnease)("...Leopold II bought it to assess Brazil as a colony. But he chose Congo instead",(getObservationValue)(2), 2, 3.9)) {
                return "?";
              }
              else {
                return "Iris Versicolour";
              }
            }
            else {
              return "Iris Versicolour";
            }
          }
          else {
            if ((considerWithUnease)("...most specimens were acquired when permission from local communities wasn’t required…",(getObservationValue)(1), 1, 2.7)) {
              if ((considerWithUnease)("...this seems to change with frameworks such as the Nagoya Protocol and the convention on biological diversity",(getObservationValue)(2), 2, 5.0)) {
                return "?";
              }
              else {
                return "Iris Virginica";
              }
            }
            else {
              if ((considerWithUnease)("…and there does not seem to be project to give them back to the countries they were collected from",(getObservationValue)(2), 2, 5.1)) {
                return "?";
              }
              else {
                return "Iris Virginica";
              }
            }
          }
        }
      }
    private:
    };
  }
}