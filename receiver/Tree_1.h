#pragma once
#include <cstdarg>
namespace PublishingHouse
{
  namespace RandomForest
  {
    class DecisionTree
    {
    public:
      const char* predict(float (*getObservationValue)(int), bool (*consider)(float, int, float))
      {
        if ((consider)((getObservationValue)(2), 2, 3.3)) {
          if ((consider)((getObservationValue)(2), 2, 1.4)) {
            if ((consider)((getObservationValue)(3), 3, 0.1)) {
              return "Iris Setosa";
            }
            else {
              return "Iris Setosa";
            }
          }
          else {
            return "Iris Setosa";
          }
        }
        else {
          if ((consider)((getObservationValue)(2), 2, 4.8)) {
            if ((consider)((getObservationValue)(3), 3, 1.0)) {
              return "Iris Versicolour";
            }
            else {
              return "Iris Versicolour";
            }
          }
          else {
            if ((consider)((getObservationValue)(2), 2, 4.9)) {
              if ((consider)((getObservationValue)(0), 0, 6.2)) {
                return "Iris Versicolour";
              }
              else {
                return "Iris Virginica";
              }
            }
            else {
              if ((consider)((getObservationValue)(3), 3, 1.8)) {
                if ((consider)((getObservationValue)(3), 3, 1.7)) {
                  if ((consider)((getObservationValue)(0), 0, 6.3)) {
                    return "Iris Virginica";
                  }
                  else {
                    return "Iris Virginica";
                  }
                }
                else {
                  return "Iris Versicolour";
                }
              }
              else {
                return "Iris Virginica";
              }
            }
          }
        }
      }
    private:
    };
  }
}