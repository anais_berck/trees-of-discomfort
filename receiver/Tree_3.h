#pragma once
#include <cstdarg>
namespace PublishingHouse
{
  namespace RandomForest
  {
    class DecisionTree
    {
    public:
      const char* predict(float (*getObservationValue)(int), bool (*consider)(float, int, float))
      {
        if ((consider)((getObservationValue)(2), 2, 3.5)) {
          if ((consider)((getObservationValue)(1), 1, 4.0)) {
            if ((consider)((getObservationValue)(1), 1, 3.0)) {
              if ((consider)((getObservationValue)(2), 2, 1.4)) {
                return "Iris Setosa";
              }
              else {
                return "Iris Setosa";
              }
            }
            else {
              return "Iris Setosa";
            }
          }
          else {
            return "Iris Setosa";
          }
        }
        else {
          if ((consider)((getObservationValue)(3), 3, 1.8)) {
            if ((consider)((getObservationValue)(0), 0, 5.7)) {
              if ((consider)((getObservationValue)(0), 0, 5.2)) {
                return "Iris Versicolour";
              }
              else {
                return "Iris Versicolour";
              }
            }
            else {
              return "Iris Versicolour";
            }
          }
          else {
            return "Iris Virginica";
          }
        }
      }
    private:
    };
  }
}