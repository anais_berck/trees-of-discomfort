#pragma once
#include <cstdarg>
namespace PublishingHouse
{
  namespace RandomForest
  {
    class TreeOfUnease
    {
    public:
      const char* traverse(float (*getObservationValue)(int), bool (*considerWithUnease)(const char*, float, int, float))
      {
        if ((considerWithUnease)("I have a thought about the Meise herbarium...",(getObservationValue)(2), 2, 3.3)) {
          if ((considerWithUnease)("...about their « living herbarium »...",(getObservationValue)(0), 0, 5.1)) {
            if ((considerWithUnease)("...some of plants there come from Leopold II his garden:",(getObservationValue)(3), 3, 0.2)) {
              if ((considerWithUnease)("...they don’t specify it, but you can infer it from their signs",(getObservationValue)(3), 3, 0.1)) {
                return "Iris Setosa";
              }
              else {
                return "Iris Setosa";
              }
            }
            else {
              return "Iris Setosa";
            }
          }
          else {
            return "Iris Setosa";
          }
        }
        else {
          if ((considerWithUnease)("...about the creation of its herbarium...",(getObservationValue)(3), 3, 1.8)) {
            if ((considerWithUnease)("...the basis of the Meise Collection is an herbarium of the plants from Brazil...",(getObservationValue)(3), 3, 1.6)) {
              if ((considerWithUnease)("...Leopold II bought it to assess Brazil as a colony. But he chose Congo instead",(getObservationValue)(0), 0, 6.1)) {
                return "?";
              }
              else {
                return "Iris Versicolour";
              }
            }
            else {
              if ((considerWithUnease)("...JSB Pohl contributed to it with 1479 specimens. He also brought back a pair of indigenous people.",(getObservationValue)(1), 1, 3.3)) {
                return "Iris Virginica";
              }
              else {
                return "Iris Versicolour";
              }
            }
          }
          else {
            if ((considerWithUnease)("...most specimens were acquired when permission from local communities wasn’t required…",(getObservationValue)(2), 2, 4.9)) {
              if ((considerWithUnease)("...this seems to change with frameworks such as the Nagoya Protocol and the convention on biological diversity",(getObservationValue)(0), 0, 6.0)) {
                return "Iris Versicolour";
              }
              else {
                return "Iris Virginica";
              }
            }
            else {
              return "Iris Virginica";
            }
          }
        }
      }
    private:
    };
  }
}