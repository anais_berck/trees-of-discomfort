#pragma once
#include <cstdarg>
namespace PublishingHouse
{
  namespace RandomForest
  {
    class DecisionTree
    {
    public:
      const char* predict(float (*getObservationValue)(int), bool (*consider)(float, int, float))
      {
        if ((consider)((getObservationValue)(3), 3, 1.0)) {
          if ((consider)((getObservationValue)(1), 1, 3.0)) {
            if ((consider)((getObservationValue)(0), 0, 4.4)) {
              return "Iris Setosa";
            }
            else {
              return "Iris Setosa";
            }
          }
          else {
            return "Iris Setosa";
          }
        }
        else {
          if ((consider)((getObservationValue)(3), 3, 1.8)) {
            if ((consider)((getObservationValue)(3), 3, 1.5)) {
              if ((consider)((getObservationValue)(3), 3, 1.4)) {
                if ((consider)((getObservationValue)(1), 1, 2.5)) {
                  if ((consider)((getObservationValue)(3), 3, 1.0)) {
                    return "Iris Versicolour";
                  }
                  else {
                    return "Iris Versicolour";
                  }
                }
                else {
                  return "Iris Versicolour";
                }
              }
              else {
                return "Iris Versicolour";
              }
            }
            else {
              if ((consider)((getObservationValue)(1), 1, 3.0)) {
                if ((consider)((getObservationValue)(0), 0, 6.2)) {
                  return "Iris Virginica";
                }
                else {
                  if ((consider)((getObservationValue)(0), 0, 6.3)) {
                    return "Iris Versicolour";
                  }
                  else {
                    if ((consider)((getObservationValue)(2), 2, 5.1)) {
                      if ((consider)((getObservationValue)(3), 3, 1.5)) {
                        return "Iris Versicolour";
                      }
                      else {
                        return "Iris Versicolour";
                      }
                    }
                    else {
                      return "Iris Virginica";
                    }
                  }
                }
              }
              else {
                if ((consider)((getObservationValue)(2), 2, 5.8)) {
                  if ((consider)((getObservationValue)(2), 2, 4.5)) {
                    return "Iris Versicolour";
                  }
                  else {
                    return "Iris Versicolour";
                  }
                }
                else {
                  return "Iris Virginica";
                }
              }
            }
          }
          else {
            return "Iris Virginica";
          }
        }
      }
    private:
    };
  }
}