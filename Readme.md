# A Council of trees

Code is meant to run on ESP-32 boards.

The sender broadcasts an observation, over WiFi.

On receiving the data the receivers classify trees using a decision tree model.
