"""
  Parses the spreadsheet into a tree structure
"""

import csv
import os.path
import json

columns = 4
position = [ 0 for _ in range(columns) ]
tree = []

def parse_column_chunk (column, start):
  label = data[start][column]
  end = start
  children = []

  while end + 1 < len(data) and (data[end + 1][column] == label or data[end + 1][column] ==  ''):
    end += 1

  if column + 1 < columns:
    child_end = start

    while child_end <= end and child_end < len(data):
      _, child_end, child = parse_column_chunk(column + 1, child_end)
      child_end += 1
      children.append(child)
    
  return (start, end, { 'label': label, 'children': children})

basepath = os.path.abspath(os.path.dirname(__file__))

with open(os.path.join(basepath, 'data.csv'), 'r') as csvfile:
  reader = csv.reader(csvfile)
  data = [row for row in reader]

  start = 0

  while start < len(data):
    _, end, node = parse_column_chunk(0, start)
    start = end + 1
    tree.append(node)

  json.dump(tree, open(os.path.join(basepath, 'unease.json'), 'w'), ensure_ascii=False)